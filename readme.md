# shintech/ts-next

## Table of Contents
1. [ Synopsis ](#synopsis)
2. [ Installation/Configuration ](#install) <br />
	a. [.env ](#env) <br />
	b. [Development ](#development) <br />
	c. [Production ](#production) <br />
3. [ Usage ](#usage) <br />

<a name="synopsis"></a>
### Synopsis

Next.JS seed project...
  
<a name="install"></a>
### Installation

    ./install.sh

<a name="env"></a>
#### Copy the config for the environment from config/testenv and edit as necessary.

    NODE_ENV=production
    NEXT_PUBLIC_GRAPHQL_URI=https://dev.shintech.ninja/api/graphql
    
<a name="development"></a>
#### Development

    npm run dev

    # or

    yarn dev

<a name="production"></a>
#### Production
    docker-compose build && docker-compose up -d
    
<a name="usage"></a>
### Usage
