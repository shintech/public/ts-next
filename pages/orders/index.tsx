import Head from 'next/head';
import { useQuery, gql } from '@apollo/client';
import Card from '@components/Card';
import { initializeApollo, addApolloState } from '@api/client';
import Feed from '@components/Feed';
import Layout from '@components/Layout';
import { GetServerSideProps, NextPage } from 'next';
import Link from 'next/link';
import { ORDER_PROPS } from '@lib/fragments';
import { defaultRoutes, defaultSidebarRoutes } from '@lib/routes';
import Loader from '@components/Loader';
import Modal from '@components/Modal';
import useModal from '@hooks/useModal';
import { useTheme } from 'styled-components';
import Check from '@public/images/check-circle.svg';

const QUERY = gql`
  ${ORDER_PROPS}

  query OrderFeed($pageSize: Int, $after: String) {
    orderFeed(pageSize: $pageSize, after: $after) {
      cursor
      hasMore
      response {
        _id
        ...OrderProps
      }
    }
  }
`;

/**
 * @description OrdersPage
 */

export default function OrdersPage () {
  const theme = useTheme();
  const { initialize, close, modal } = useModal();

  const { data, loading, fetchMore, error } = useQuery(QUERY, {
    fetchPolicy: 'cache-first',
    variables: {
      pageSize: 12
    }
  });

  if (error) return <Card message='Failed to Load'/>;
  if (loading) return <Loader className='loading-message' message='Loading...'/>;

  const loadMore = () =>
    fetchMore({
      variables: {
        after: data.orderFeed.cursor,
        pageSize: 18
      }
    });

  const orderFeed = data?.orderFeed;

  const tableColumns = [
    {
      name: 'verified'
    },
    {
      name: 'message'
    },
    {
      name: 'status'
    }
  ];

  return (
    <>
      <Head>
        <title>New Order</title>
        <meta name="viewport" content="viewport-fit=cover" />
      </Head>

      <div>
        <Link href='/orders/new'><a>New Order</a></Link>
        <Feed
          hasMore={orderFeed.hasMore}
          columns={tableColumns}
          rows={orderFeed.response}
          loadMore={loadMore}
          onClickCard={initialize}
        />
      </div>

      {
        modal.open &&
        <Modal handleClose={close}>
          <h1>{modal.props.message}</h1>
          <ul>
            <li>Verified: <Check fill={modal.props.verified ? theme.colors.success.main : theme.colors.error.main} height={10} width={10}/></li>
            <li>Status: {modal.props.status}</li>
          </ul>

        </Modal>
      }
    </>
  );
}

OrdersPage.getLayout = function getLayout (page: NextPage) {
  return (
    <Layout routes={defaultRoutes} sidebarRoutes={defaultSidebarRoutes}>
      {page}
    </Layout>
  );
};

export const getServerSideProps: GetServerSideProps = async ({ req }) => {
  const apolloClient = initializeApollo();

  await apolloClient.query({
    query: QUERY,
    context: {
      headers: req.headers
    }
  });

  return addApolloState(apolloClient, {
    props: {
      initialApolloState: apolloClient.cache.extract()
    }
  });
};
