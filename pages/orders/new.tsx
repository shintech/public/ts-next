import Head from 'next/head';
import { useMutation, gql } from '@apollo/client';
import { initializeApollo, addApolloState } from '@api/client';
import DynamicForm from '@components/DynamicForm';
import Layout from '@components/Layout';
import { GetServerSideProps, NextPage } from 'next';
import { ORDER_PROPS } from '@lib/fragments';
import * as yup from 'yup';
import { defaultRoutes, defaultSidebarRoutes } from '@lib/routes';
import useForm from '@hooks/useForm';

const MUTATION = gql`
  ${ORDER_PROPS}
  
  mutation CreateOrder($body: OrderPostBody) {
    createOrder(body: $body) {
      _id
      ...OrderProps
    }
  }
`;

/**
 * @description NewOrderPage
 */

export default function NewOrderPage () {
  const [submitRequest] = useMutation(MUTATION, {
    update (cache, { data: { createOrder } }) {
      cache.modify({
        fields: {
          orderFeed (cachedFeed = []) {
            const newRef = cache.writeFragment({
              data: createOrder,
              fragmentName: 'NewOrder',
              fragment: gql`
                ${ORDER_PROPS}

                fragment NewOrder on OrderFeed {
                  cursor
                  hasMore
                  response {
                    _id
                    ...OrderProps
                  }
                }
              `
            });

            return {
              ...cachedFeed,
              response: [
                newRef,
                ...cachedFeed.response
              ]
            };
          }
        }
      });
    }
  });

  const formInputs = [
    { name: 'message', type: 'text', placeholder: 'Placeholder', required: true },
    { name: 'status', type: 'number', required: true },
    { name: 'verified', type: 'checkbox' }
  ];

  const schema = yup.object({
    message: yup
      .string()
      .min(4)
      .max(10)
      .required('Message is required'),
    status: yup
      .number()
      .typeError('Status must be a number')
      .min(0)
      .max(10)
      .required('Status is required'),
    verified: yup
      .boolean()
      .optional()
  });

  const { onSubmit } = useForm({
    mutation: submitRequest,
    redirect: '/orders'
  });

  return (
    <>
      <Head>
        <title>New Order</title>
        <meta name="viewport" content="viewport-fit=cover" />
      </Head>

      <DynamicForm
        title='Create Order'
        fields={formInputs}
        schema={schema}
        onSubmit={onSubmit}
      />
    </>
  );
}

NewOrderPage.getLayout = function getLayout (page: NextPage) {
  return (
    <Layout routes={defaultRoutes} sidebarRoutes={defaultSidebarRoutes}>
      {page}
    </Layout>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  const apolloClient = initializeApollo();

  return addApolloState(apolloClient, {
    props: {
      initialApolloState: apolloClient.cache.extract()
    }
  });
};
