import Head from 'next/head';
import { useMutation, gql } from '@apollo/client';
import { initializeApollo, addApolloState } from '@api/client';
import DynamicForm from '@components/DynamicForm';
import Layout from '@components/Layout';
import { GetServerSideProps, NextPage } from 'next';
import { USER_PROPS } from '@lib/fragments';
import * as yup from 'yup';
import { defaultRoutes, defaultSidebarRoutes } from '@lib/routes';
import useForm from '@hooks/useForm';

const MUTATION = gql`
  ${USER_PROPS}

  mutation CreateUser($body: UserPostBody) {
    createUser(body: $body) {
      __typename
      _id
      ...UserProps
    }
  }
`;

/**
 * @description NewUserPage
 */

export default function NewUserPage () {
  const [submitRequest] = useMutation(MUTATION, {
    update (cache, { data: { createUser } }) {
      cache.modify({
        fields: {
          userFeed (cachedFeed = []) {
            const newRef = cache.writeFragment({
              data: createUser,
              fragmentName: 'NewUser',
              fragment: gql`
                ${USER_PROPS}

                fragment NewUser on UserFeed {
                  cursor
                  hasMore
                  response {
                    _id
                    ...UserProps
                  }
                }
              `
            });

            return {
              ...cachedFeed,
              response: [
                newRef,
                ...cachedFeed.response
              ]
            };
          }
        }
      });
    }
  });

  const { onSubmit } = useForm({
    mutation: submitRequest,
    redirect: '/users'
  });

  const formInputs = [
    { name: 'firstName', type: 'text', placeholder: 'First Name', required: true },
    { name: 'lastName', type: 'text', placeholder: 'Last Name', required: true },
    { name: 'username', type: 'text', placeholder: 'Username', required: true },
    { name: 'email', type: 'text', placeholder: 'Email', required: true },
    { name: 'password', type: 'password', placeholder: 'Password', required: true }
  ];

  const schema = yup.object({
    firstName: yup
      .string()
      .required('First Name is required'),
    lastName: yup
      .string()
      .required('Last Name is required'),
    username: yup
      .string()
      .required('Username is required'),
    email: yup
      .string()
      .email()
      .required('Email is required'),
    password: yup
      .string()
      .required('Password is required')
  });

  return (
    <>
      <Head>
        <title>New User</title>
        <meta name="viewport" content="viewport-fit=cover" />
      </Head>

      <DynamicForm
        title='Create User'
        fields={formInputs}
        schema={schema}
        onSubmit={onSubmit}
      />
    </>
  );
}

NewUserPage.getLayout = function getLayout (page: NextPage) {
  return (
    <Layout routes={defaultRoutes} sidebarRoutes={defaultSidebarRoutes}>
      {page}
    </Layout>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  const apolloClient = initializeApollo();

  return addApolloState(apolloClient, {
    props: {
      initialApolloState: apolloClient.cache.extract()
    }
  });
};
