import Head from 'next/head';
import { useQuery, gql } from '@apollo/client';
import Card from '@components/Card';
import { initializeApollo, addApolloState } from '@api/client';
import Feed from '@components/Feed';
import Layout from '@components/Layout';
import { GetServerSideProps, NextPage } from 'next';
import Link from 'next/link';
import { USER_PROPS } from '@lib/fragments';
import { defaultRoutes, defaultSidebarRoutes } from '@lib/routes';
import Loader from '@components/Loader';
import Modal from '@components/Modal';
import useModal from '@hooks/useModal';

const QUERY = gql`
  ${USER_PROPS}

  query UserFeed($pageSize: Int, $after: String) {
    userFeed(pageSize: $pageSize, after: $after) {
      cursor
      hasMore
      response {
        _id
        ...UserProps
      }
    }
  }
`;

/**
 * @description UsersPage
 */

export default function UsersPage () {
  const { initialize, close, modal } = useModal();

  const { data, loading, fetchMore, error } = useQuery(QUERY, {
    fetchPolicy: 'cache-first',
    variables: {
      pageSize: 12
    }
  });

  if (error) return <Card message='Failed to Load'/>;
  if (loading) return <Loader className='loading-message' message='Loading...'/>;

  const loadMore = () =>
    fetchMore({
      variables: {
        after: data.userFeed.cursor,
        pageSize: 18
      }
    });

  const userFeed = data?.userFeed;

  const tableColumns = [
    {
      name: 'fullName',
      label: 'Name'
    },
    {
      name: 'email'
    },
    {
      name: 'username'
    }
  ];

  return (
    <>
      <Head>
        <title>Users Index</title>
        <meta name="viewport" content="viewport-fit=cover" />
      </Head>

      <div>
        <Link href='/users/new'><a>New User</a></Link>
        <Feed
          hasMore={userFeed.hasMore}
          columns={tableColumns}
          rows={userFeed.response}
          loadMore={loadMore}
          onClickCard={initialize}
        />
      </div>

      {
        modal.open &&
        <Modal handleClose={close}>
          <h1>{modal.props.fullName}</h1>
          <ul>
            <li>Email: {modal.props.email}</li>
            <li>Username: {modal.props.username}</li>
          </ul>

        </Modal>
      }
    </>
  );
}

UsersPage.getLayout = function getLayout (page: NextPage) {
  return (
    <Layout routes={defaultRoutes} sidebarRoutes={defaultSidebarRoutes}>
      {page}
    </Layout>
  );
};

export const getServerSideProps: GetServerSideProps = async ({ req }) => {
  const apolloClient = initializeApollo();

  await apolloClient.query({
    query: QUERY,
    context: {
      headers: req.headers
    }
  });

  return addApolloState(apolloClient, {
    props: {
      initialApolloState: apolloClient.cache.extract()
    }
  });
};
