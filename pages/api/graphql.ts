import { ApolloServer } from 'apollo-server-micro';
import { RedisCache } from 'apollo-server-cache-redis';
import dataSources from '@api/dataSources';
import orderTypes from '@api/type-defs/order';
import orderResolver from '@api/resolvers/orders';
import infoTypes from '@api/type-defs/info';
import infoResolver from '@api/resolvers/info';
import userTypes from '@api/type-defs/users';
import userResolver from '@api/resolvers/users';
import logger from '@lib/logger';
import {
  GraphQLRequestContext,
  GraphQLRequestListener
} from 'apollo-server-plugin-base';
import { RequestHandler } from 'micro';
import { isEmpty } from 'lodash';
import connectDb from '@lib/mongo';

const NODE_ENV = process.env['NODE_ENV'];
const REDIS_HOST = process.env['REDIS_HOST'] || '127.0.0.1';
const REDIS_PORT = process.env['REDIS_PORT'] || '6379';
const NEXT_PUBLIC_GRAPHQL_URI = process.env['NEXT_PUBLIC_GRAPHQL_URI'] || 'http://localhost:3000/api/graphql';

const isProd = NODE_ENV === 'production';

const cache = new RedisCache({
  host: REDIS_HOST,
  port: parseInt(REDIS_PORT)
});

const apolloServer = new ApolloServer({
  dataSources,
  typeDefs: [
    orderTypes,
    infoTypes,
    userTypes
  ],
  resolvers: [
    orderResolver,
    infoResolver,
    userResolver
  ],
  debug: !isProd,
  cache,
  persistedQueries: {
    cache
  },

  context: async (ctx) => {
    const headers = ctx.req.headers;

    return { headers };
  },

  plugins: [
    {
      async requestDidStart (
        { request }: GraphQLRequestContext
      ): Promise<GraphQLRequestListener | void> {
        if (request.operationName !== 'IntrospectionQuery') {
          logger.info(`graphql: ${request.operationName || 'Undefined Operation'}...`);

          if (request.query) { logger.info(`graphql: ${request.query.trim()}`); }
          if (!isEmpty(request.variables)) { logger.info(`graphql - variables: ${JSON.stringify(request.variables, null, 2)}`); }
        }
      }
    }
  ]
});

export const config = {
  api: {
    bodyParser: false
  }
};

const startServer = apolloServer.start();

connectDb();

const app: RequestHandler = async (req, res) => {
  const allowedOrigins = ['https://studio.apollographql.com', NEXT_PUBLIC_GRAPHQL_URI];

  const origin: string | undefined = req.headers.origin;

  if (origin && allowedOrigins.includes(origin)) {
    res.setHeader('Access-Control-Allow-Origin', origin);
  }

  res.setHeader('Access-Control-Allow-Credentials', 'true');

  res.setHeader(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Methods, Access-Control-Allow-Origin, Access-Control-Allow-Credentials, Access-Control-Allow-Headers'
  );

  res.setHeader(
    'Access-Control-Allow-Methods',
    'POST, GET, PUT, PATCH, DELETE, OPTIONS, HEAD'
  );

  if (req.method === 'OPTIONS') {
    res.end();
    return false;
  }

  await startServer;
  await apolloServer.createHandler({
    path: '/api/graphql'
  })(req, res);
};

export default app;
