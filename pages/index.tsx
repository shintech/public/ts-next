import Head from 'next/head';
import { useQuery, gql } from '@apollo/client';
import Card from '@components/Card';
import { initializeApollo, addApolloState } from '@api/client';
import Layout from '@components/Layout';
import { GetServerSideProps, NextPage } from 'next';
import { defaultRoutes, defaultSidebarRoutes } from '@lib/routes';

const QUERY = gql`
  query IndexQuery {
    info  {
      name,
      version
    }
  }
`;

/**
 * @description IndexPage
 * @returns {IndexPage}
 */

export default function IndexPage () {
  const { data, loading, error } = useQuery(QUERY);

  if (error) return <Card message='Failed to Load'/>;
  if (loading) return <Card message='Loading...'/>;

  return (
    <>
      <Head>
        <title>{data.info.name} | version: {data.info.version}</title>
        <meta name="viewport" content="viewport-fit=cover" />
      </Head>

      <div className='version-card'>
        <Card message={`${data.info.name} version: ${data.info.version}`}/>
      </div>
    </>
  );
}

IndexPage.getLayout = function getLayout (page: NextPage) {
  return (
    <Layout routes={defaultRoutes} sidebarRoutes={defaultSidebarRoutes}>
      {page}
    </Layout>
  );
};

export const getServerSideProps: GetServerSideProps = async ({ req }) => {
  const apolloClient = initializeApollo();

  const { data } = await apolloClient.query({
    query: QUERY,
    context: {
      headers: req.headers
    }
  });

  return addApolloState(apolloClient, {
    props: {
      titleHeading: data.info.name,
      titleMessage: `version: ${data.info.version}`,
      initialApolloState: apolloClient.cache.extract()
    }
  });
};
