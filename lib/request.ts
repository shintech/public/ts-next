import http from 'http';

interface HttpRequestOptions {
  method?: string,
  body?: object,
  headers?: {
    [key: string]: string
  }
}

const request = (url:string, httpOptions: HttpRequestOptions) => new Promise((resolve, reject) => {
  const { method = 'GET', body, headers } = httpOptions;
  const { hostname, pathname, port } = new URL(url);

  const options = {
    hostname,
    port,
    path: pathname,
    method,
    headers
  };

  const chunks: Uint8Array[] = [];

  const req = http.request(options, res => {
    res.on('data', chunk => {
      chunks.push(chunk);
    });

    res.on('end', () => {
      const status = res.statusCode;
      const body = JSON.parse(Buffer.concat(chunks).toString());

      resolve({
        status,
        body
      });
    });
  });

  req.on('error', reject);

  if (body && headers?.['Content-Type'] === 'application/json') {
    req.write(JSON.stringify(body));
  }

  req.end();
});

export default request;
