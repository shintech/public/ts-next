import mongoose from 'mongoose';
import logger from './logger';

const MONGO_HOST = process.env['MONGO_HOST'];
const MONGO_PORT = process.env['MONGO_PORT'];
const MONGO_INITDB_DATABASE = process.env['MONGO_INITDB_DATABASE'];
const MONGO_INITDB_ROOT_USERNAME = process.env['MONGO_INITDB_ROOT_USERNAME'];
const MONGO_INITDB_ROOT_PASSWORD = process.env['MONGO_INITDB_ROOT_PASSWORD'];
const MONGO_AUTH_SOURCE = process.env['MONGO_AUTH_SOURCE'] || 'admin';

const MONGO_URI = `mongodb://${MONGO_INITDB_ROOT_USERNAME}:${MONGO_INITDB_ROOT_PASSWORD}@${MONGO_HOST}:${MONGO_PORT}/${MONGO_INITDB_DATABASE}`;

const connectDb = async () => {
  try {
    await mongoose.connect(String(MONGO_URI), {
      authSource: MONGO_AUTH_SOURCE
    });
    logger.info('connected to database');
  } catch (err) {
    logger.error('error connecting to database');
    logger.error(err);
    process.exit(1);
  }
};

export default connectDb;
