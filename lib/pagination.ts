interface PaginateResultsProps {
  after: string;
  pageSize: number;
  results: Array<any>;
}

export const paginateResults = ({ after: cursor, pageSize, results }: PaginateResultsProps) => {
  if (pageSize < 1) return [];
  if (!cursor) return results.slice(0, pageSize);

  const cursorIndex = results.findIndex(item => item.cursor || null ? cursor === item.cursor || null : false);

  return cursorIndex >= 0
    ? cursorIndex === results.length - 1
      ? []
      : results.slice(
        cursorIndex + 1,
        Math.min(results.length, cursorIndex + 1 + pageSize)
      )
    : results.slice(0, pageSize);
};
