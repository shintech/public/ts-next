import { ThemeInterface } from '@typings/theme';

export const theme: ThemeInterface = {
  spacing: (index = 0) => {
    const spacing = [
      '0.25rem',
      '0.5rem',
      '0.75rem',
      '1rem',
      '1.5rem',
      '2rem',
      '3rem',
      '4rem',
      '6rem',
      '8rem',
      '12rem',
      '16rem',
      '20rem'
    ];

    return spacing[index];
  },

  colors: {
    primary: {
      main: '#07ff51d2',
      dark: '#024616d2',
      light: '#a9f1bfd2',
      text: '#000000b5'
    },
    secondary: {
      main: '#c93ff3',
      dark: '#491758',
      light: '#dcb2e9',
      text: '#ffffff'
    },
    background: {
      main: 'lightgreen',
      dark: 'darkgreen',
      light: 'ghostwhite'
    },
    error: {
      main: 'red',
      text: 'red'
    },
    success: {
      main: 'green',
      text: '#000000b5'
    }
  }
};
