export interface NavigationRoute {
  name?: string;
  path: `/${string}`;
}

export const defaultRoutes: NavigationRoute[] = [
  {
    path: '/'
  },
  {
    path: '/users'
  },
  {
    path: '/orders'
  }
];

export const defaultSidebarRoutes: NavigationRoute[] = [
  {
    path: '/users/new',
    name: 'Create User'
  },

  {
    path: '/orders/new',
    name: 'Create Order'
  }
];
