import { gql } from '@apollo/client';

export const USER_PROPS = gql`
  fragment UserProps on User {
    fullName
    email
    username
  }
`;

export const ORDER_PROPS = gql`
  fragment OrderProps on Order {
    message
    status
    verified
  }
`;
