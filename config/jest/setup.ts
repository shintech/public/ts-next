import path from 'path';
import * as dotenv from 'dotenv';
import '@testing-library/jest-dom/extend-expect';

dotenv.config({
  path: path.join(process.cwd(), 'config', 'jest', 'testenv')
});
