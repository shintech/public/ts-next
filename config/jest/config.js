/* eslint-disable @typescript-eslint/no-var-requires */
/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
const { pathsToModuleNameMapper } = require('ts-jest');
const { compilerOptions } = require('../../tsconfig');

const ignorePatterns = [
  '<rootDir>/node_modules/',
  '<rootDir>/archive'
];

const setupFilesAfterEnv = [
  '<rootDir>/config/jest/setup.ts'
];

module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'jsdom',
  testPathIgnorePatterns: ignorePatterns,
  watchPathIgnorePatterns: ignorePatterns,
  rootDir: process.cwd(),
  setupFilesAfterEnv,
  testRegex: 'spec\\.tsx$',
  moduleNameMapper: pathsToModuleNameMapper(compilerOptions.paths, { prefix: '<rootDir>' }),
  globals: {
    'ts-jest': {
      tsconfig: 'tsconfig.test.json'
    }
  }
};
