import mongoose from 'mongoose';

const { Schema } = mongoose;

mongoose.Promise = global.Promise;

export interface IOrder extends mongoose.Document {
  message: string;
  status: number;
  verified: boolean;
}

const OrdersSchema: mongoose.Schema = new Schema({
  cursor: {
    type: String,
    required: true,
    default: () => new Date().getTime(),
    trim: true
  },
  message: {
    type: String,
    required: true,
    trim: true
  },
  status: {
    type: Number,
    required: true,
    trim: true
  },
  verified: {
    type: Boolean,
    required: true,
    trim: true
  }
}, {
  timestamps: {
    createdAt: true,
    updatedAt: true
  }
});

// OrdersSchema.index({ name: 'text' })

const Order: mongoose.Model<IOrder> = mongoose.models.Order || mongoose.model('Order', OrdersSchema);

export default Order;
