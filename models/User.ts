import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
import logger from '@lib/logger';

const { Schema } = mongoose;

mongoose.Promise = global.Promise;

export interface IUser extends mongoose.Document {
  cursor: string;
  firstName: string;
  lastName: string;
  username: string;
  email: string;
  password: string
}

export const generateHash = (password: string, saltRounds = 10) => {
  try {
    return bcrypt.hash(password, saltRounds);
  } catch (err: unknown) {
    if (err instanceof Error) {
      return err.message;
    } else {
      throw err;
    }
  }
};

// const UserAddress = {
//   addressLineOne: {
//     type: String,
//     required: true,
//     trim: true
//   },
//   addressLineTwo: {
//     type: String,
//     required: false,
//     trim: true
//   },
//   addressLineThree: {
//     type: String,
//     required: false,
//     trim: true
//   },
//   city: {
//     type: String,
//     required: true,
//     trim: true
//   },
//   state: {
//     type: String,
//     required: false,
//     trim: true
//   },
//   zip: {
//     type: String,
//     required: true,
//     trim: true
//   }
// };

// const json = {
// "firstName": "String",
// "lastName": "String",
// "username": "string",
// "email": "String",
// "password": "string",
// "address": {
// "primary": {
// "addressLineOne": "String",
// "city": "String",
// "state": "String",
// "zip": "String"
// }
// }
// };
//

const UsersSchema: mongoose.Schema = new Schema({
  cursor: {
    type: String,
    required: true,
    default: () => new Date().getTime(),
    trim: true
  },
  firstName: {
    type: String,
    required: true,
    trim: true
  },
  lastName: {
    type: String,
    required: true,
    trim: true
  },
  username: {
    type: String,
    required: true,
    trim: true,
    unique: true,
    dropDups: true
  },
  email: {
    type: String,
    required: true,
    trim: true,
    unique: true,
    dropDups: true
  },
  password: {
    type: String,
    required: true,
    trim: false
  },
  createdAt: {
    required: true,
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  }
}, {
  timestamps: {
    createdAt: true,
    updatedAt: true
  }
});

UsersSchema.virtual('fullName')
  .get(function () { return `${this.firstName} ${this.lastName}`; })
  .set(function (v) {
    // `v` is the value being set, so use the value to set
    // `firstName` and `lastName`.
    const firstName = v.substring(0, v.indexOf(' '));
    const lastName = v.substring(v.indexOf(' ') + 1);
    this.set({ firstName, lastName });
  });

// UsersSchema.query.authenticate = async function (username, password) {
//   if (!username || username === '' || !password || password === '') {
//     throw new Error(`authentication failure - username/password are required`);
//   }

//   const generateWebToken = ({ _id, username }) =>
//     jwt.sign({ _id, username }, JWT_SECRET, {
//       expiresIn: 60 * 60 * 24
//     });

//   try {
//     const user = await this.where({ username });

//     const authenticated = bcrypt.compareSync(password, user.password);

//     if (authenticated) {
//       const token = generateWebToken(user);

//       return ({
//         token,
//         message: `authentication success - username:${username}`
//       });
//     } else {
//       throw new Error(`authentication failure - username:${username}`);
//     }
//   } catch (err) {
//     throw new Error(err);
//   }
// };

const User: mongoose.Model<IUser> = mongoose.models.User || mongoose.model('User', UsersSchema);

export default User;
