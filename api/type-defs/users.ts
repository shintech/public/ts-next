import { gql } from '@apollo/client';

export default gql`
  enum SortOptions {
    ascending,
    descending
  }

  type User {
    _id: ID!
    firstName: String!
    lastName: String!
    fullName: String!
    username: String!
    email: String!
    cursor: String!
    updatedAt: String!
    createdAt: String!
  }

  input UserSearchInput {
    _id: ID
    firstName: String
    lastName: String
    fullName: String
    username: String
    email: String
    cursor: String
    updatedAt: String
    createdAt: String
  }

  input OrderSortInput {
    _id: SortOptions
    firstName: SortOptions
    lastName: SortOptions
    fullName: SortOptions
    username: SortOptions
    email: SortOptions
    cursor: SortOptions
    updatedAt: SortOptions
    createdAt: SortOptions
  }

  input UserPostBody {
    "First Name"
    firstName: String!
    "Last Name"
    lastName: String!
    "Username"
    username: String!
    "Email"
    email: String!
    "Password"
    password: String!
  }

  type UserFeed {
    cursor: String!
    hasMore: Boolean!
    response: [User]!
  }

  input UserSearchProps {
    where: UserSearchInput
    sort: OrderSortInput
  }

  type Query {
    user(_id: ID!): User!
    userFeed(
      "If you add a cursor here, it will only return results _after_ this cursor"
      after: String

      "The number of results to show. Must be >= 1. Default = 20"
      pageSize: Int

      "Search properties"
      searchProps: UserSearchProps
    ): UserFeed!
  }

  type Mutation {
    createUser(body: UserPostBody): User!
    updateUser(_id: String, body: UserPostBody): User!
    deleteUser(_id: String): User!
  }
`;
