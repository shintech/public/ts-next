import { gql } from '@apollo/client';

export default gql`
  type Info {
    _id: ID!
    name: String
    version: String
  }

  type Query {
    info: Info!
  }
`;
