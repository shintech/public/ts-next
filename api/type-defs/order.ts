import { gql } from '@apollo/client';

export default gql`
  enum SortOptions {
    ascending,
    descending
  }

  type Order {
    _id: ID!
    message: String!
    status: Int!
    verified: Boolean!
    cursor: String!
    updatedAt: String!
    createdAt: String!
  }

  input OrderSearchInput {
    _id: ID
    message: String
    status: Int
    verified: Boolean
    cursor: String
    updatedAt: String
    createdAt: String
  }

  input OrderSortInput {
    _id: SortOptions
    message: SortOptions
    status: SortOptions
    verified: SortOptions
    cursor: SortOptions
    updatedAt: SortOptions
    createdAt: SortOptions
  }

  input OrderPostBody {
    "A number to display"
    status: Int!

    "A message string to display"
    message: String!

    "A boolean to display"
    verified: Boolean!
  }

  type OrderFeed {
    cursor: String!
    hasMore: Boolean!
    response: [Order]!
  }

  input SearchProps {
    where: OrderSearchInput
    sort: OrderSortInput
  }

  type Query {
    order(_id: ID!): Order!
    orderFeed(
      "If you add a cursor here, it will only return results _after_ this cursor"
      after: String

      "The number of results to show. Must be >= 1. Default = 20"
      pageSize: Int

      "Search properties"
      searchProps: SearchProps
    ): OrderFeed!
  }

  type Mutation {
    createOrder(body: OrderPostBody): Order!
    updateOrder(_id: String, body: OrderPostBody): Order!
    deleteOrder(_id: String): Order!
  }
`;
