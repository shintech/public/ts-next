import { DataSource } from 'apollo-datasource';
import { Callback, HydratedDocument, Model, Query, Types } from 'mongoose';
import logger from '@lib/logger';

// import { KeyValueCache } from 'apollo-server-caching';

export interface SearchProps {
  where?: Callback<any[]> | undefined;
  sort?: Record<string, 'ascending' | 'descending'>;
}

const defaultSearchProps: SearchProps = {
  sort: { _id: 'descending' }
};

export default class DataSourceTemplate extends DataSource {
  model: Model<any>;
  // cache: any;

  constructor (model: Model<any>) {
    super();
    this.model = model;
  }

  // initialize ({ cache }: { cache: KeyValueCache<string> }) {
  //   this.cache = cache;
  // }

  /**
   * @description Fetch all documents using the provided model.
   * @param {Record<string, any>} where
   * @returns {Query<any, HydratedDocument<any>> | string }
   */
  findAll ({ where, sort }: SearchProps = defaultSearchProps): Query<any, HydratedDocument<any>> | string {
    try {
      return this.model.find(where).sort(sort);
    } catch (err: unknown) {
      if (err instanceof Error) {
        logger.error(err);
        return err.message;
      } else {
        throw err;
      }
    }
  }

  /**
   * @description Fetch one document using the provided model and _id
   * @param {Types.ObjectId} _id A mongoose ObjectId
   * @returns {Query<any, HydratedDocument<any>> | string}
   */
  findById (
    _id: Types.ObjectId
  ): Query<any, HydratedDocument<any>> | string {
    try {
      return this.model.findById(_id);
    } catch (err: unknown) {
      if (err instanceof Error) {
        logger.error(err);
        return err.message;
      } else {
        throw err;
      }
    }
  }

  /**
 * @description Fetch one document using the provided model
 * @param {Types.ObjectId} _id A mongoose ObjectId
 * @returns {Query<any, HydratedDocument<any>> | string}
 */
  findOne (
    { where }: SearchProps
  ): Query<any, HydratedDocument<any>> | string {
    try {
      return this.model.findOne(where);
    } catch (err: unknown) {
      if (err instanceof Error) {
        logger.error(err);
        return err.message;
      } else {
        throw err;
      }
    }
  }

  async create (
    props: Record<string, any>
  ): Promise<Query<any, HydratedDocument<any>> | string> {
    try {
      return this.model.create(props);
    } catch (err: unknown) {
      if (err instanceof Error) {
        logger.error(err);
        return err.message;
      } else {
        throw err;
      }
    }
  }

  async update (
    _id: Types.ObjectId,
    props: Record<string, any>
  ): Promise<Query<any, HydratedDocument<any>> | string> {
    const Document = this.model;

    try {
      const doc = await Document.findOneAndUpdate({ _id }, props, {
        new: true
      });

      if (!doc) {
        throw new Error('not found');
      }

      return doc;
    } catch (err: unknown) {
      if (err instanceof Error) {
        logger.error(err);
        return err.message;
      } else {
        throw err;
      }
    }
  }

  async delete (
    _id: Types.ObjectId
  ): Promise<Query<any, HydratedDocument<any>> | string> {
    const Document = this.model;

    try {
      const doc = await Document.findByIdAndRemove(_id);

      if (!doc) {
        throw new Error('not found');
      }

      return doc;
    } catch (err: unknown) {
      if (err instanceof Error) {
        logger.error(err);
        return err.message;
      } else {
        throw err;
      }
    }
  }
}
