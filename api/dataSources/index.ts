import DataSource from '@api/dataSources/template';

import User from '@models/User';
import Order from '@models/Order';

const dataSources = () => ({
  users: new DataSource(User),
  orders: new DataSource(Order)
});

export default dataSources;
