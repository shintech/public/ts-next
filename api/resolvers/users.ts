import { IUser, generateHash } from '../../models/User';
import { ApolloError } from 'apollo-server-errors';
import logger from '@lib/logger';
import { paginateResults } from '@lib/pagination';
import { Types } from 'mongoose';
import { AppContext, QueryResolverArgs, FeedResponse } from '@typings/apollo';

/* Query ############################################################ */

export interface QueryResolver {
  user: (_: unknown, body: { _id: Types.ObjectId }, context: AppContext) => Promise<IUser | ApolloError>;
  userFeed: (_: unknown, body: QueryResolverArgs, context: AppContext) => Promise<FeedResponse>;
}

const queryResolver: QueryResolver = {
  user: async (_, { _id }, { dataSources }) => {
    try {
      return dataSources.users.findById(_id);
    } catch (err: unknown) {
      if (err instanceof Error) {
        logger.error(err);
        throw new ApolloError(err.message);
      } else {
        throw err;
      }
    }
  },

  userFeed: async (_, { after, pageSize = 20, searchProps }, { dataSources }) => {
    const allUsers = await dataSources.users.findAll(searchProps);

    const users = paginateResults({
      after,
      pageSize,
      results: allUsers
    });

    return {
      response: users,
      cursor: users.length ? users[users.length - 1].cursor : null,
      hasMore: users.length
        ? users[users.length - 1].cursor !==
          allUsers[allUsers.length - 1].cursor
        : false
    };
  }
};

/* Mutation ############################################################ */

export interface ResolverArgs {
  body: IUser
}

export interface MutationResolver {
  createUser: (_: unknown, body: ResolverArgs, context: AppContext) => Promise<IUser | ApolloError>;
  updateUser: (_: unknown, body: { _id: Types.ObjectId } & ResolverArgs, context: AppContext) => Promise<IUser | ApolloError>;
  deleteUser: (_: unknown, body: { _id: Types.ObjectId }, context: AppContext) => Promise<IUser | ApolloError>;
}

const mutationResolver: MutationResolver = {
  createUser: async (_, { body }, { dataSources }) => {
    try {
      const newDoc = {
        ...body,
        password: await generateHash(body.password)
      };

      return dataSources.users.create(newDoc);
    } catch (err: unknown) {
      if (err instanceof Error) {
        logger.error(err);
        throw new ApolloError(err.message);
      } else {
        throw err;
      }
    }
  },

  updateUser: async (_, { _id, body }, { dataSources }) => {
    try {
      return dataSources.users.update(_id, body);
    } catch (err: unknown) {
      if (err instanceof Error) {
        logger.error(err);
        throw new ApolloError(err.message);
      } else {
        throw err;
      }
    }
  },

  deleteUser: async (_, { _id }, { dataSources }) => {
    try {
      return dataSources.users.delete(_id);
    } catch (err: unknown) {
      if (err instanceof Error) {
        logger.error(err);
        throw new ApolloError(err.message);
      } else {
        throw err;
      }
    }
  }
};

const resolvers = {
  Query: queryResolver,
  Mutation: mutationResolver
};

export default resolvers;
