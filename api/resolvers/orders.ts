import { IOrder } from '../../models/Order';
import { ApolloError } from 'apollo-server-errors';
import logger from '@lib/logger';
import { paginateResults } from '@lib/pagination';
import { Types } from 'mongoose';
import { AppContext, QueryResolverArgs, FeedResponse } from '@typings/apollo';

/* Query ############################################################ */

export interface QueryResolver {
  order: (_: unknown, body: { _id: Types.ObjectId }, context: AppContext) => Promise<IOrder | ApolloError>;
  orderFeed: (_: unknown, body: QueryResolverArgs, context: AppContext) => Promise<FeedResponse>;
}

const queryResolver: QueryResolver = {
  order: async (_, { _id }, { dataSources }) => {
    try {
      return dataSources.orders.findById(_id);
    } catch (err: unknown) {
      if (err instanceof Error) {
        logger.error(err);
        throw new ApolloError(err.message);
      } else {
        throw err;
      }
    }
  },

  orderFeed: async (_, { after, pageSize = 20, searchProps }, { dataSources }) => {
    const allOrders = await dataSources.orders.findAll(searchProps);

    const orders = paginateResults({
      after,
      pageSize,
      results: allOrders
    });

    return {
      response: orders,
      cursor: orders.length ? orders[orders.length - 1].cursor : null,
      hasMore: orders.length
        ? orders[orders.length - 1].cursor !==
          allOrders[allOrders.length - 1].cursor
        : false
    };
  }
};

/* Mutation ############################################################ */

export interface ResolverArgs {
  body: IOrder
}

export interface MutationResolver {
  createOrder: (_: unknown, body: ResolverArgs, context: AppContext) => Promise<IOrder | ApolloError>;
  updateOrder: (_: unknown, body: { _id: Types.ObjectId } & ResolverArgs, context: AppContext) => Promise<IOrder | ApolloError>;
  deleteOrder: (_: unknown, body: { _id: Types.ObjectId }, context: AppContext) => Promise<IOrder | ApolloError>;
}

const mutationResolver: MutationResolver = {
  createOrder: async (_, { body }, { dataSources }) => {
    try {
      return dataSources.orders.create(body);
    } catch (err: unknown) {
      if (err instanceof Error) {
        logger.error(err);
        throw new ApolloError(err.message);
      } else {
        throw err;
      }
    }
  },

  updateOrder: async (_, { _id, body }, { dataSources }) => {
    try {
      return dataSources.orders.update(_id, body);
    } catch (err: unknown) {
      if (err instanceof Error) {
        logger.error(err);
        throw new ApolloError(err.message);
      } else {
        throw err;
      }
    }
  },

  deleteOrder: async (_, { _id }, { dataSources }) => {
    try {
      return dataSources.orders.delete(_id);
    } catch (err: unknown) {
      if (err instanceof Error) {
        logger.error(err);
        throw new ApolloError(err.message);
      } else {
        throw err;
      }
    }
  }
};

const resolvers = {
  Query: queryResolver,
  Mutation: mutationResolver
};

export default resolvers;
