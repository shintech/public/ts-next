const PKG_NAME = process.env['npm_package_name'];
const PKG_VERSION = process.env['npm_package_version'];

/* Query ############################################################ */

export interface InfoResponse {
  name: string | undefined;
  version: string | undefined;
}

export interface QueryResolver {
  info: () => InfoResponse
}

const Query: QueryResolver = {
  info: () => ({
    name: PKG_NAME,
    version: PKG_VERSION
  })
};

const resolvers = {
  Query
};

export default resolvers;
