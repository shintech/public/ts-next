import { HttpLink } from '@apollo/client/link/http';
import { onError } from '@apollo/client/link/error';
import { createPersistedQueryLink } from '@apollo/client/link/persisted-queries';
import { sha256 } from 'crypto-hash';
import fetch from 'isomorphic-unfetch';

export const createHttpLink = (uri: string) => {
  return new HttpLink({
    uri,
    credentials: 'same-origin',
    fetch
  });
};

export const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors) {
    graphQLErrors.forEach(({ message, locations, path }) =>
      // eslint-disable-next-line no-console
      console.log(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`)
    );
  }

  // eslint-disable-next-line no-console
  if (networkError) console.log(`[Network error]: ${networkError}`);
});

export const persistedQueriesLink = createPersistedQueryLink({ sha256 });
