import { useMemo } from 'react';
import { ApolloClient, InMemoryCache, from, NormalizedCacheObject } from '@apollo/client';
import { createHttpLink, errorLink, persistedQueriesLink } from '@api/client/links';
import isEqual from 'lodash/isEqual';
import merge from 'deepmerge';

import type { AppProps } from 'next/app';

type InitialState = NormalizedCacheObject | undefined;

let apolloClient: ApolloClient<NormalizedCacheObject> | undefined;

const APOLLO_STATE_PROP_NAME = '__APOLLO_STATE__';
const uri = process.env['NEXT_PUBLIC_GRAPHQL_URI'] || 'http://localhost:3000/api/graphql';

const createApolloClient = () => {
  return new ApolloClient({
    ssrMode: typeof window === 'undefined',
    link: from([errorLink, persistedQueriesLink, createHttpLink(uri)]),
    cache: new InMemoryCache({
      typePolicies: {
        Query: {
          fields: {
            userFeed: {
              // Don't cache separate results based on
              // any of this field's arguments.
              keyArgs: false,
              merge (existing = { response: [] }, incoming) {
                return {
                  __typename: 'UserFeed',
                  cursor: incoming.cursor,
                  hasMore: incoming.hasMore,
                  response: [ ...existing.response, ...incoming.response ]
                };
              }
            },

            orderFeed: {
              // Don't cache separate results based on
              // any of this field's arguments.
              keyArgs: false,
              merge (existing = { response: [] }, incoming) {
                console.log('existing', existing);
                return {
                  __typename: 'OrderFeed',
                  cursor: incoming.cursor,
                  hasMore: incoming.hasMore,
                  response: [ ...existing.response, ...incoming.response ]
                };
              }
            }
          }
        }
      }
    })
  });
};

export const initializeApollo = (initialState?: InitialState) => {
  const _apolloClient = apolloClient ?? createApolloClient();

  if (initialState) {
    const existingCache = _apolloClient.extract();

    const data = merge(initialState, existingCache, {
      arrayMerge: (destinationArray, sourceArray) => [
        ...sourceArray,
        ...destinationArray.filter((d) =>
          sourceArray.every((s) => !isEqual(d, s))
        )
      ]
    });

    _apolloClient.cache.restore(data);
  }

  if (typeof window === 'undefined') return _apolloClient;
  if (!apolloClient) apolloClient = _apolloClient;

  return _apolloClient;
};

export const addApolloState = (
  client: ApolloClient<NormalizedCacheObject>,
  pageProps: AppProps['pageProps']
) => {
  if (pageProps?.props) {
    pageProps.props[APOLLO_STATE_PROP_NAME] = client.cache.extract();
  }

  return pageProps;
};

export const useApollo = (pageProps: AppProps['pageProps']) => {
  const state = pageProps[APOLLO_STATE_PROP_NAME];
  const store = useMemo(() => initializeApollo({ initialState: state }), [
    state
  ]);
  return store;
};
