# ts-next
# Stage 1 - Base
FROM node:16.14.0 AS base
WORKDIR /home/node
COPY package.json yarn.lock ./
RUN wget -O /usr/local/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v1.2.2/dumb-init_1.2.2_amd64 && \
  chmod +x /usr/local/bin/dumb-init

# 
# Stage 2 - Dependencies
FROM base AS dependencies
RUN yarn install --production=true
RUN cp -R node_modules prod_node_modules
RUN yarn install

# 
# Stage 3 - Build
FROM base as builder
COPY --from=dependencies /home/node/node_modules ./node_modules
COPY --chown=node:node . .
RUN yarn build
RUN rm -rf node_modules

# 
# Stage 4 - Release
FROM builder as release
COPY --from=dependencies /home/node/prod_node_modules ./node_modules
COPY --from=builder --chown=node:node /home/node .
USER node
ENTRYPOINT ["dumb-init", "--"]
CMD yarn start
