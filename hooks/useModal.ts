import { useState } from 'react';

interface ModalProps {
  open: boolean;
  props: Record<string, any>;
}

interface UseModalReturn {
  initialize: (props: Record<string, any>) => void;
  toggle: (open: boolean) => void;
  close: () => void;
  modal: ModalProps;
}

const useModal: () => UseModalReturn = () => {
  const [modal, setModal] = useState<ModalProps>({ open: false, props: {} });

  return {
    modal,

    initialize: (props) => {
      setModal({
        props,
        open: true
      });
    },

    toggle: (currentState) => {
      setModal({
        ...modal,
        open: !currentState
      });
    },

    close: () => {
      setModal({
        ...modal,
        open: false
      });
    }
  };
};

export default useModal;
