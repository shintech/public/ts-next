import { ApolloCache, ApolloError, DefaultContext, MutationFunctionOptions, OperationVariables } from '@apollo/client';
import { useRouter } from 'next/router';
import { toast } from 'react-toastify';

interface UseFormProps {
  mutation: (options?: MutationFunctionOptions<any, OperationVariables, DefaultContext, ApolloCache<any>> | undefined) => Promise<any>;
  redirect?: string;
}

export default function useForm ({ mutation, redirect = '/' }: UseFormProps) {
  const router = useRouter();

  const onSubmit = async (props: Record<string, any>) => {
    try {
      const variables = {
        body: {
          ...props
        }
      };

      await toast.promise(
        mutation({
          variables,

          context: {
            headers: {
              'content-type': 'application/json'
            }
          }
        }),

        {
          pending: {
            icon: true,
            render: () => 'Pending!'
          },
          success: {
            icon: true,
            render: () => 'Success!'
          },
          error: {
            icon: true,
            render: ({ data }) => <div>{data.message}</div>
          }
        }
      );

      router.push(redirect);
    } catch (err: unknown) {
      if (err instanceof ApolloError) {
        // eslint-disable-next-line
        console.error(err.message);
      }
    }
  };

  return {
    onSubmit
  };
}
