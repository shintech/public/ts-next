import Link from 'next/link';
import ActiveLink from '@components/ActiveLink';
import { NavigationRoute } from '@lib/routes';
import Styled from './styled';
import BrandIcon from '@public/images/nodejs.svg';

const defaultRoutes: NavigationRoute[] = [
  {
    path: '/'
  }
];

interface NavBarProps {
  routes?: NavigationRoute[];
}

/**
 * @description Navigation Bar
 * @param {NavigationRoute} props.routes An array of navigation routes to display in the navbar.
 */

export default function Navbar ({ routes = defaultRoutes }: NavBarProps) {
  return (
    <Styled className='navbar'>
      <div className='brand'>
        <Link href='/'>
          <a><BrandIcon className='brand-icon'/></a>
        </Link>
      </div>

      <ul className='nav-links'>
        {
          routes.map(route =>
            <li key={route.name || route.path}>
              <ActiveLink activeClassName="active" href={route.path}>
                <a>{((route.path === '/')) ? 'home' : route.name || route.path.substring(1).toLowerCase()}</a>
              </ActiveLink>
            </li>
          )
        }
      </ul>
    </Styled>
  );
}
