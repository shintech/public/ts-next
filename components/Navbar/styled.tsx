import styled from 'styled-components';

/**
 * @description Navbar Style Component
 */

const Styled = styled.nav`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;

  background-color: ${props => props.theme.colors.primary.dark};
  padding: 0 ${props => props.theme.spacing(2)};

  svg {
    height: ${props => props.theme.spacing(5)};
  }

  .nav-links {
    display: flex;
    flex-direction: row;
    
    list-style: none;
    height: 100%;

    li { 
      height: 100%;
      border-left: 1px solid black;

      &:last-child {
        border-right: 1px solid black;
      }
    }

    li a { 
      height: 100%;
      padding: 0 ${props => props.theme.spacing(3)};
      display:flex;
      align-items: center;
      text-decoration: none;

      background-color: ${props => props.theme.colors.secondary.main};
      color: ${props => props.theme.colors.secondary.text};

      &:hover {
        cursor: pointer;
        background-color: ${props => props.theme.colors.primary.main};
        color: ${props => props.theme.colors.primary.text};
      }

      &.active {
        background-color: ${props => props.theme.colors.secondary.dark};

        &:hover {
          cursor: not-allowed;
          background-color: ${props => props.theme.colors.primary.light};
          color: ${props => props.theme.colors.primary.text};
        }
      }
    }
  }
`;

export default Styled;
