import Styled from './styled';

interface Props {
  message: string;
}

const Card = ({ message }: Props) => <Styled data-testid="card">{message}</Styled>;

export default Card;
