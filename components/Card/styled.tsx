import styled from 'styled-components';

const Styled = styled.h1`
  border: 1px solid black;
  border-radius: 5px;
  padding: 2ch;
  transition: .2s ease-out;
  background-color: ${props => props.theme.colors.primary.main};
  color: ${props => props.theme.colors.primary.text};

  &:hover {
    cursor: pointer;
    transition: .2s ease-out;
    background-color: ${props => props.theme.colors.secondary.main};
    color: ${props => props.theme.colors.secondary.text};
    border-color: ${props => props.theme.colors.secondary.main};
  }
`;

export default Styled;
