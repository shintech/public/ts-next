import Styled from './styled';

interface LoaderProps {
  message: string;
  className: string;
}

const Loader = ({ message, className }: LoaderProps) => <Styled className={className} data-testid="error-block">{message}</Styled>;

export default Loader;
