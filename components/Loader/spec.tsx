/* eslint-env jest */

import { render, waitFor, screen, cleanup } from '@testing-library/react';
import { ThemeProvider } from 'styled-components';
import React from 'react';
import Card from '@components/Card';
import { theme } from '@lib/theme';
import '@testing-library/jest-dom';

const props = {
  message: 'message'
};

describe('COMPONENT -> Card -> Mock properties...', () => {
  afterEach(cleanup);

  it(`expect heading text to equal "${props.message}"...`, async () => {
    render(
      <ThemeProvider theme={theme}>
        <Card { ...props }/>
      </ThemeProvider>
    );

    await waitFor(() => screen.getByRole('heading'));

    expect(screen.getByRole('heading')).toHaveTextContent(props.message);
  });
});
