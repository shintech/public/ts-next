import { titleize } from 'inflected';
import { ObjectID } from 'bson';
import Check from '@public/images/check-circle.svg';
import Styled from './styled';

interface Column {
  name: string;
  label?: string;
}

interface TableProps {
  columns: Column[];
  rows: Array<{ _id: ObjectID } & Record<string, any>>;
}

export default function Table ({ columns, rows }: TableProps) {
  const headers = columns.map(column => {
    return (
      <th key={column.name}>{column.label || titleize(column.name)}</th>
    );
  });

  return (
    <Styled>
      <thead>
        <tr>
          {headers}
        </tr>
      </thead>

      <tbody>
        {
          rows.map((row: Record<string, any>, index: number) =>
            <tr key={index}>
              {
                columns.map(column => {
                  const cellData = row[column.name];

                  if (typeof cellData === 'boolean') {
                    return (
                      <td key={`${row._id}${column.name}`}>
                        {cellData ? <Check fill='green' height={10} width={10}/> : ''}
                      </td>
                    );
                  }

                  return (
                    <td key={`${row._id}${column.name}`}>{row[column.name]}</td>
                  );
                })
              }
            </tr>
          )
        }
      </tbody>
    </Styled>
  );
}
