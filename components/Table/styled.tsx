import styled from 'styled-components';

/**
 * @description Table Style Component
 */

const Styled = styled.table`
  td, th {
    padding: 1ch;
  }

  &, tr, th, td{
    border: 1px solid black;
    border-collapse: collapse;
  }

  td {
    text-align: center;
  }

  thead {
    background-color: ${props => props.theme.colors.primary.main};
  }

  tr:nth-child(even) {
    background-color: lightgray;
  }
`;

export default Styled;
