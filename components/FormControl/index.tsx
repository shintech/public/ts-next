import Styled from './styled';
import { ReactNode } from 'react';
import { humanize } from 'inflected';

export interface FormControlProps {
  name?: string;
  children: ReactNode;
  error?: string;
}

/**
 * @description Creates a form-control block that displays a help block under a child input.
 * @param {String} props.name The optional name of the child input prop. Will not display errors if no name is provided.
 * @param {ReactNode} props.children The input to be rendered in the FormControl component.
 * @param {String} props.error The optional error message to display in the help block
 * @returns
 */

const FormControl = ({ name, children, error }: FormControlProps) => {
  return (
    <Styled error={Boolean(error)} className='form-control'>
      {
        children
      }

      <div data-testid='help-block' className='help-block'>
        { error && name ? `${humanize(error)}!` : <br/> }
      </div>
    </Styled>
  );
};

export default FormControl;
