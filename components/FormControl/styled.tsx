import styled from 'styled-components';

type FormControlStyleProps = {
  error: boolean;
}

/**
 * @param {Boolean} error
 */
const Styled = styled.div<FormControlStyleProps>`
  margin: 1ch 0;

  & .help-block {
    margin: 5px 0;
    text-align: right;
    color: ${(props) => props.error ? props.theme.colors.error.main : props.theme.colors.success.main};
  }

  input {
    padding: 1ch 2ch;

    &:not([type="reset"]):not([type="submit"]) {
      width: 100%;
    }

    &:not(:last-child) {
      margin-right: 2ch;
    }
  }
`;

export default Styled;
