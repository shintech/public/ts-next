/* eslint-env jest */

import { render, waitFor, screen, cleanup } from '@testing-library/react';
import { ThemeProvider } from 'styled-components';
import React from 'react';
import Component from '@components/FormControl';
import { theme } from '@lib/theme';
import '@testing-library/jest-dom';

const props = {
  name: 'message',
  error: 'Error'
};

describe('COMPONENT -> FormControl -> With error...', () => {
  afterEach(cleanup);

  it(`expect h1 text to equal "${props.name}"...`, async () => {
    render(
      <ThemeProvider theme={theme}>
        <Component { ...props }>
          <input type="text" />
        </Component>
      </ThemeProvider>
    );

    await waitFor(() => screen.getByTestId('help-block'));

    expect(screen.getByTestId('help-block')).toHaveTextContent(props.error);
  });
});
