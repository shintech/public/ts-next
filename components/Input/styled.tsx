import styled from 'styled-components';
import { HTMLInputTypeAttribute } from 'react';

interface InputStyleProps {
  type?: HTMLInputTypeAttribute | undefined;
  required?: boolean;
  error: boolean
}

/**
 * @description Input Style Component
 * @param {Boolean} required
 */

const Styled = styled.div<InputStyleProps>`
  label {
    position: relative;
    color: ${props => props?.error ? props.theme.colors.error.main : props?.theme.colors.primary.text}
  }

  label span {
    color: ${props => props?.required ? props.theme.colors.error.main : props.theme.colors.primary.text};
  }

  input[type=checkbox], input[type=radio] {
    position: absolute;
    bottom: 0.25ch;
  }

  input[type=radio] { 
    bottom: 2px; 
  }

  input:not([type="checkbox"]):not([type="radio"]),
  input:not([type="checkbox"]):not([type="radio"]):focus {
    margin-top: 5px;
    outline-style: solid;
    outline-width: 1px;
    outline-color: ${props => props?.error ? props.theme.colors.error.main : props?.theme.colors.primary.text}
  }
`;

export default Styled;
