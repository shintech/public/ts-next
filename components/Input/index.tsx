/* eslint-disable @typescript-eslint/ban-ts-comment */

import { titleize } from 'inflected';
// @ts-ignore
import { AllHTMLAttributes, useId } from 'react';
import { useController, UseControllerProps } from 'react-hook-form';
import Styled from './styled';

export interface CustomInputProps<T> extends AllHTMLAttributes<T> {
  name: string;
  error?: boolean;
}

export default function CustomInput (props: UseControllerProps<Record<string, any>> & CustomInputProps<Record<string, any>>) {
  const { field } = useController(props);

  const labelId = useId();

  return (
    <Styled
      type={props.type}
      required={Boolean(props.required)}
      error={Boolean(props.error)}
    >
      <label htmlFor={labelId}>
        {props?.label || titleize(props.name)}&nbsp;<span>{props.required && '*'}</span>

        <input
          type={props.type}
          { ...field }
        />
      </label>
    </Styled>
  );
}
