import styled from 'styled-components';

const Styled = styled.div`
  /* The Modal (background) */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */

  /* Modal Content/Box */
  .modal-body {
    background-color: #fefefe;
    border: 1px solid ${props => props.theme.colors.primary.text};
    border-radius: ${props => props.theme.spacing(2)};
    margin: 15% auto; /* 15% from the top and centered */
    padding: 25px;
    min-height: 35vh;
    border: 1px solid #888;
    width: 56vw; /* Could be more or less, depending on screen size */
    position: relative;
  }

  /* The Close Button */
  .close {
    color: #aaa;
    font-weight: bold;
    position: absolute;
    right:    0;
    top:   0;
    font-size: 2ch;
    text-align: center;
    margin: ${props => props.theme.spacing(1)}
  }

  .close:hover,
  .close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
  }
`;

export default Styled;
