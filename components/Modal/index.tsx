import Styled from './styled';
import { ReactNode } from 'react';

export interface ModalProps {
  children: ReactNode;
  handleClose: () => void;
}

/**
 * @description Creates a form-control block that displays a help block under a child input.
 * @param {String} props.name The optional name of the child input prop. Will not display errors if no name is provided.
 * @param {ReactNode} props.children The input to be rendered in the FormControl component.
 * @param {String} props.error The optional error message to display in the help block
 * @returns
 */

const Modal = ({ children, handleClose }: ModalProps) => {
  return (
    <Styled className='modal'>
      <div className='modal-body'>
        <div className='modal-content'>
          {
            children
          }
        </div>

        <div className='modal-controls'>
          <button className='close' onClick={handleClose}>Close</button>
        </div>

      </div>
    </Styled>
  );
};

export default Modal;
