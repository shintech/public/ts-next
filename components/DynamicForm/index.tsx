import { HTMLInputTypeAttribute } from 'react';
import { FieldValues, RegisterOptions, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import FormControl from '@components/FormControl';
import CustomInput from '@components/Input';
import Styled from './styled';

export interface FieldProps {
  name: string;
  label?: string;
  type?: HTMLInputTypeAttribute | 'switch' | undefined;
  registerOptions?: RegisterOptions;
}

export interface FormProps {
  title: string;
  fields: Array<FieldProps>;
  onSubmit: (props: Record<string, any>) => void;
  schema: any
}

/**
 *
 * @param {String} title A title to display on the form
 * @param {Array<FieldProps>} fields An array of input options used to create the inputs.
 * @param {(props: Record<string, any>) => void} onSubmit The function to execute when the submit button is pressed.
 * @param schema The Yup validation schema.
 * @returns
 */

const DynamicForm = ({ title, fields, schema, onSubmit }: FormProps) => {
  const defaultValues: FieldValues = fields.reduce((acc, field) => {
    let value;

    switch (field.type) {
      case 'text':
        value = '';
        break;

      case 'number':
        value = 0;
        break;

      case 'checkbox':
        value = false;
        break;

      default:
        value = '';
        break;
    }

    return {
      ...acc,
      [field.name]: value
    };
  }, {});

  const hook = useForm({
    resolver: yupResolver(schema),
    defaultValues,
    mode: 'onChange'
  });

  const {
    handleSubmit,
    reset,
    control,
    formState: {
      isSubmitting,
      errors
    }
  } = hook;

  return (
    <Styled
      noValidate
      data-testid="form"
      onSubmit={handleSubmit(onSubmit)}
    >
      <fieldset>
        <legend><b>{title}</b></legend>

        {
          fields.map((field) => {
            return (
              <FormControl
                key={field.name}
                name={field.name}
                error={errors[field.name]?.message}
              >
                <CustomInput
                  { ...field }
                  name={field.name}
                  rules={{ required: true }}
                  control={control}
                  error={Boolean(errors[field.name]?.message)}
                />
              </FormControl>
            );
          })
        }
      </fieldset>

      <FormControl>
        <input
          disabled={isSubmitting}
          type="submit"
        />

        <input
          disabled={isSubmitting}
          type="reset"
          onClick={() => reset(defaultValues, {
            keepErrors: false,
            keepDirty: false,
            keepIsSubmitted: false,
            keepTouched: false,
            keepIsValid: false,
            keepSubmitCount: false
          })}
        />
      </FormControl>
    </Styled>);
};

export default DynamicForm;
