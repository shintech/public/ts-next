import styled from 'styled-components';

const Styled = styled.form`
  width: 20vw;

  fieldset {
    background-color: ${props => props.theme.colors.primary.light};
    padding: 2ch;
    border-radius: 5px;

    & legend {
      font-size: 2.5ch;
    }
  }
`;

export default Styled;
