import Styled from './styled';
import Navbar from '@components/Navbar';
import { ReactNode } from 'react';
import { NavigationRoute } from '@lib/routes';
import Sidebar from '@components/Sidebar';

interface LayoutProps {
  routes: NavigationRoute[];
  sidebarRoutes: NavigationRoute[];
  children: ReactNode
}

export default function Layout ({ routes, sidebarRoutes, children }: LayoutProps) {
  return (
    <Styled>
      <Navbar routes={routes} />

      <Sidebar routes={sidebarRoutes}/>

      <main>
        {children}
      </main>
    </Styled>
  );
}
