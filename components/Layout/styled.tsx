import styled from 'styled-components';

/**
 * @description Navbar Style Component
 * @param {Boolean} required
 */

const Styled = styled.div`
  min-height: 100vh;
  background-color: ${props => props.theme.colors.background.main};
  display: grid;
  grid-template-rows: 50px 1fr 30px;
  grid-template-columns: 155px 1fr;
  grid-template-areas:
    'header header header header header header'
    'menu main main main main right'
    'menu footer footer footer footer footer';

  .navbar {
    grid-area: header;
    
  }

  .sidebar {
    grid-area: menu;
    position: sticky;
    top: 0;
  }

  main {
    display: flex;
    justify-content: center;
    grid-area: main;
    margin-top: ${props => props.theme.spacing(5)};

    .loading-message, .version-card {
      position: absolute;
      top: 50%;
      left: 50%;
      -webkit-transform: translateX(-50%);
      transform: translateX(-50%)
    }
  }
`;

export default Styled;
