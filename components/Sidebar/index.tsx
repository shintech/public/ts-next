import ActiveLink from '@components/ActiveLink';
import { NavigationRoute } from '@lib/routes';
import Styled from './styled';

const defaultRoutes: NavigationRoute[] = [
  {
    path: '/',
    name: 'Home'
  }
];

interface SidebarProps {
  routes?: NavigationRoute[];
}

/**
 * @description Sidebar
 * @param {NavigationRoute} props.routes An array of navigation routes to display in the sidebar.
 */

export default function Sidebar ({ routes = defaultRoutes }: SidebarProps) {
  return (
    <Styled className='sidebar'>
      <div className="sidebar">
        <ul className='nav-links'>
          {
            routes.map(route => {
              return (
                <li key={route.name || route.path}>
                  <ActiveLink activeClassName="active" href={route.path}>
                    <a>{((route.path === '/')) ? 'home' : route.name || route.path.substring(1).toLowerCase()}</a>
                  </ActiveLink>
                </li>
              );
            }
            )
          }
        </ul>
      </div>
    </Styled>
  );
}
