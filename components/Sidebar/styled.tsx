import styled from 'styled-components';

/**
 * @description Sidebar Style Component
 */

const Styled = styled.nav`
  background-color: ${props => props.theme.colors.secondary.main};

  li {
    border-color: black;
    border-style: solid;
    border-width: 0 1px 1px 1px;
    background-color: ${props => props.theme.colors.secondary.light};

    & .active {
      background-color: ${props => props.theme.colors.secondary.dark};
      color: ${props => props.theme.colors.secondary.light};
    }

    a {
      display: block;
      text-decoration: none;
      height: 100%;
      margin: 0;
      padding: ${props => props.theme.spacing(2)};
    }
  }
`;

export default Styled;
