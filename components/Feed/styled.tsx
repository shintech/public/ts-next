import styled from 'styled-components';

/**
 * @description Table Style Component
 */

const Styled = styled.div`
  .container {
    display: flex;
    white-space: pre-wrap;
    flex-direction: row;
    flex-wrap: wrap;
    width: 54vw;
    border: 1px solid ${props => props.theme.colors.primary.text};
    border-radius: 5px;
    background-color: ${props => props.theme.colors.background.light};
  }

  .card {
    border: 1px solid black;
    border-radius: 5px;
    width: ${props => props.theme.spacing(12)};
    background-color: ${props => props.theme.colors.secondary.light};
    padding: ${props => props.theme.spacing(3)};
    margin: ${props => props.theme.spacing(2)};

    &:hover {
      cursor: pointer;
      background-color: ${props => props.theme.colors.primary.light};
    }

    & p {
      margin: ${props => props.theme.spacing(2)}
    }
  }

  .has-more {
      width: 100%;
      text-align: center;
  }

  button {
      background-color: ${props => props.theme.colors.secondary.dark};
      color: ${props => props.theme.colors.secondary.text};
      padding: ${props => props.theme.spacing(2)};
      margin-top: ${props => props.theme.spacing(2)};
      border-radius: 5px;

      &:hover {
        cursor: pointer;
        background-color: ${props => props.theme.colors.primary.light};
        color: ${props => props.theme.colors.primary.text};
      }
    }
`;

export default Styled;
