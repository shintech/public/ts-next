import { ObjectID } from 'bson';
import Styled from './styled';
import { useTheme } from 'styled-components';
import Check from '@public/images/check-circle.svg';

interface Column {
  name: string;
  label?: string;
}

interface FeedProps {
  columns: Column[];
  rows: Array<{ _id: ObjectID } & Record<string, any>>;
  hasMore: boolean;
  loadMore: () => void;
  onClickCard?: (props: any) => void;
}

export default function Feed ({ columns, rows, hasMore, loadMore, onClickCard = () => null }: FeedProps) {
  const theme = useTheme();

  return (
    <>
      <Styled>
        <div className='container'>
          {
            rows.map((row: Record<string, any>, index: number) =>
              <div className='card' onClick={() => onClickCard(row)} key={index}>
                {
                  columns.map(column => {
                    const cellData = row[column.name];

                    if (typeof cellData === 'boolean') {
                      return (
                        <p key={`${row._id}${column.name}`}>
                          <b>{column.name}</b>:&nbsp;
                          <Check fill={cellData ? theme.colors.success.main : theme.colors.error.main} height={10} width={10}/>
                        </p>
                      );
                    }

                    return (
                      <p key={`${row._id}${column.name}`}><b>{column.name}</b>: {row[column.name]}</p>
                    );
                  })
                }
              </div>
            )
          }
        </div>

        {
          hasMore && <div className='has-more'><button onClick={loadMore}>Load More...</button></div>
        }
      </Styled>
    </>
  );
}
