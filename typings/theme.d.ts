import 'styled-components';
import { Range } from './util';

export type ThemeSpacingIndexRange = Range<0, 13>;

export interface ThemeColorTypes {
  main: string;
  text?: string;
  dark?: string;
  light?: string;
}

export interface ThemeColors {
  primary: ThemeColorTypes;
  secondary: ThemeColorTypes;
  background: ThemeColorTypes;
  error: ThemeColorTypes;
  success: ThemeColorTypes;
}

export interface ThemeInterface {
  spacing: (index?: ThemeSpacingIndexRange) => string;
  colors: ThemeColors;
}

// type ThemeInterface = typeof theme

declare module 'styled-components' {
    interface DefaultTheme extends ThemeInterface {}
}
