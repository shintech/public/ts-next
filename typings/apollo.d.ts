import type { SearchProps } from '@api/dataSources/template';
import type DataSource from '@api/dataSources/template';

export interface AppContext {
  dataSources: {
    [key: string]: DataSource
  };
}

export interface FeedResponse {
  cursor?: string | null;
  hasMore: boolean;
  response: Record<string, any>
}

export interface QueryResolverArgs {
  after: string;
  pageSize: number;
  searchProps: SearchProps
}
