// eslint-disable-next-line @typescript-eslint/no-var-requires
const { PHASE_DEVELOPMENT_SERVER } = require('next/constants');

const eslint = {
  dirs: [
    'components',
    'graphql',
    'lib',
    'pages'
  ]
};

const webpack = (config) => {
  config.plugins = config.plugins || [];

  config.module.rules = [
    ...config.module.rules,
    {
      test: /\.svg$/,
      use: ['@svgr/webpack']
    }
  ];

  return config;
};

const compiler = {
  styledComponents: true
};

module.exports = (phase) => {
  if (phase === PHASE_DEVELOPMENT_SERVER) {
    return {
      webpack,
      eslint,
      compiler
    };
  }

  return {
    webpack,
    eslint,
    compiler
  };
};
